package main

import (
	"gitlab.com/editorservice/backend/editor/connections"
	"gitlab.com/editorservice/backend/editor/impl"
)

func main() {
	server := impl.NewServer()
	server.Serve(connections.EditorServiceUrl)
}
