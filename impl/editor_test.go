package impl

import (
	"context"
	editor "gitlab.com/editorservice/backend/editor/api"
	"gitlab.com/editorservice/backend/editor/connections"
	"gitlab.com/editorservice/backend/editor/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"testing"
	"time"
)

var EditorServiceClient editor.EditorServiceClient

func init() {
	go func() {
		server := NewServer()
		server.Serve(connections.EditorServiceUrl)
	}()
}

func createEditorServiceClient() error {
	client, err := grpc.Dial(connections.EditorServiceUrl, grpc.WithInsecure())
	EditorServiceClient = editor.NewEditorServiceClient(client)
	return err
}

func TestEditorServiceClient(t *testing.T) {
	err := createEditorServiceClient()
	if err != nil {
		t.Errorf("Error connections to Editor Service gRPC: %s", err)
		return
	}
}

func TestArticleCRUD(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	user := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(user))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	var id interface{} = primitive.NewObjectID()

	respCreateArticle, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User: user,
			Article: &editor.Article{
				Id:                id.(primitive.ObjectID).Hex(),
				Status:            "Redaction",
				Author:            user,
				Name:              "article 1",
				CreatedAt:         time.Now().Format("2 January 2006 15:04"),
				UpdatedAt:         "",
				Content:           "This is content article 1",
				Comments:          nil,
				Versions:          nil,
				TemplateId:        "",
				AttachedArticles:  nil,
				CurrentVersion:    0,
				ResponsibleEditor: nil,
				Section:           nil,
				Reviewer:          nil,
				Review:            "",
				CuratorConsultant: nil,
			},
		})
	if err != nil || respCreateArticle == nil || !respCreateArticle.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respGetArticle, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != id.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}

	articleForUpdate := &editor.Article{
		Id:                respGetArticle.Article.Id,
		Status:            "Approved",
		Author:            respGetArticle.Article.Author,
		Name:              "article 1, version 2",
		CreatedAt:         respGetArticle.Article.CreatedAt,
		UpdatedAt:         respGetArticle.Article.UpdatedAt,
		Content:           "This is content article 1, version 2",
		Comments:          respGetArticle.Article.Comments,
		Versions:          respGetArticle.Article.Versions,
		TemplateId:        respGetArticle.Article.TemplateId,
		AttachedArticles:  respGetArticle.Article.AttachedArticles,
		CurrentVersion:    respGetArticle.Article.CurrentVersion,
		ResponsibleEditor: user,
		Section:           respGetArticle.Article.Section,
		Reviewer:          user,
		Review:            "link for review",
		CuratorConsultant: user,
	}

	respEditArticle, err := EditorServiceClient.EditArticle(
		context.TODO(),
		&editor.EditArticleRequest{
			User:    user,
			Article: articleForUpdate,
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if !respEditArticle.Success {
		t.Error("Error Edit Article success")
		return
	}

	respGetArticle, err = EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})

	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != id.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticle.Article.Reviewer.Id != articleForUpdate.Reviewer.Id ||
		respGetArticle.Article.Review != articleForUpdate.Review ||
		respGetArticle.Article.Name != articleForUpdate.Name ||
		respGetArticle.Article.Content != articleForUpdate.Content ||
		respGetArticle.Article.CurrentVersion != 1 ||
		respGetArticle.Article.CuratorConsultant.Id != articleForUpdate.CuratorConsultant.Id {
		t.Error("Error Update Article result")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      user,
			ArticleId: respGetArticle.Article.Id,
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(user.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}

func TestInsertToArticle(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	user := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(user))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	var (
		articleMainID   interface{} = primitive.NewObjectID()
		articleAttachID interface{} = primitive.NewObjectID()
	)

	articleMain := &editor.Article{
		Id:        articleMainID.(primitive.ObjectID).Hex(),
		Status:    "Redaction",
		Author:    user,
		Name:      "article main",
		CreatedAt: time.Now().Format("2 January 2006 15:04"),
		Content:   "This is content article main",
	}

	articleAttach := &editor.Article{
		Id:        articleAttachID.(primitive.ObjectID).Hex(),
		Status:    "Redaction",
		Author:    user,
		Name:      "article attach",
		CreatedAt: time.Now().Format("2 January 2006 15:04"),
		Content:   "This is content article attach",
	}

	respCreateArticleMain, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User:    user,
			Article: articleMain,
		})
	if err != nil || respCreateArticleMain == nil || !respCreateArticleMain.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respCreateArticleAttach, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User:    user,
			Article: articleAttach,
		})
	if err != nil || respCreateArticleAttach == nil || !respCreateArticleAttach.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respInsertToArticle, err := EditorServiceClient.InsertToArticle(
		context.TODO(),
		&editor.InsertToArticleRequest{
			User:          user,
			NewArticle:    articleAttach,
			MainArticleId: articleMainID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Insert To Article response")
		return
	}
	if !respInsertToArticle.Success {
		t.Error("Error Insert To Article success")
		return
	}

	respGetArticleMain, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: articleMainID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticleMain == nil || respGetArticleMain.Article.Id != articleMainID.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticleMain.Article.AttachedArticles == nil {
		t.Error("Error Attached Articles results")
		return
	} else {
		if respGetArticleMain.Article.AttachedArticles[0].Id != articleAttach.Id ||
			respGetArticleMain.Article.AttachedArticles[0].Content != articleAttach.Content {
			t.Error("Error Attached Articles results")
			return
		}
	}

	respGetArticleAttach, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: articleAttachID.(primitive.ObjectID).Hex(),
		})
	if err == nil || respGetArticleAttach != nil {
		t.Error("Error delete attach article after attaching")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      user,
			ArticleId: articleMainID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(user.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}

func TestCommentArticleCRUD(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	author := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	userForCommenting := &editor.User{
		Id:         "1a282e5fas112e9ed4r31202",
		FirstName:  "Alex",
		MiddleName: "Sergeevich",
		LastName:   "Commentator",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(author))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	err = userCollection.Create(model.RenderBackUserModel(userForCommenting))
	if err != nil {
		t.Errorf("Error Create testing user (commentator): %s", err)
		return
	}

	var articleID interface{} = primitive.NewObjectID()

	article := &editor.Article{
		Id:        articleID.(primitive.ObjectID).Hex(),
		Status:    "Redaction",
		Author:    author,
		Name:      "article for commenting",
		CreatedAt: time.Now().Format("2 January 2006 15:04"),
		Content:   "This is content article for commenting",
	}

	respCreateArticle, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User:    author,
			Article: article,
		})
	if err != nil || respCreateArticle == nil || !respCreateArticle.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	var (
		firstCommentID  interface{} = primitive.NewObjectID()
		secondCommentID interface{} = primitive.NewObjectID()
	)
	respFirstCommentArticle, err := EditorServiceClient.CommentArticle(
		context.TODO(),
		&editor.CommentArticleRequest{
			User:      userForCommenting,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
			Comment: &editor.Comment{
				Id:      firstCommentID.(primitive.ObjectID).Hex(),
				Author:  userForCommenting,
				Content: "This is the first comment",
			},
		})
	if err != nil || respFirstCommentArticle == nil || !respFirstCommentArticle.Success {
		t.Errorf("Error First Comment Article response: %s", err)
		return
	}

	respSecondCommentArticle, err := EditorServiceClient.CommentArticle(
		context.TODO(),
		&editor.CommentArticleRequest{
			User:      userForCommenting,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
			Comment: &editor.Comment{
				Id:      secondCommentID.(primitive.ObjectID).Hex(),
				Author:  userForCommenting,
				Content: "This is the second comment",
			},
		})
	if err != nil || respSecondCommentArticle == nil || !respSecondCommentArticle.Success {
		t.Errorf("Error Second Comment Article response: %s", err)
		return
	}

	respGetArticle, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != articleID.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if len(respGetArticle.Article.Comments) != 2 ||
		respGetArticle.Article.Comments[0].Content != "This is the first comment" ||
		respGetArticle.Article.Comments[1].Content != "This is the second comment" {
		t.Error("Error Comment Article results")
		return
	}

	respEditCommentArticle, err := EditorServiceClient.EditCommentArticle(
		context.TODO(),
		&editor.EditCommentArticleRequest{
			User:      userForCommenting,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
			Comment: &editor.Comment{
				Id:      secondCommentID.(primitive.ObjectID).Hex(),
				Author:  userForCommenting,
				Content: "This is the second comment - editable",
			},
		})
	if err != nil || respEditCommentArticle == nil || !respEditCommentArticle.Success {
		t.Errorf("Error Edit Comment Article response: %s", err)
		return
	}

	respGetArticle, err = EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != articleID.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if len(respGetArticle.Article.Comments) != 2 ||
		respGetArticle.Article.Comments[0].Content != "This is the first comment" ||
		respGetArticle.Article.Comments[1].Content != "This is the second comment - editable" {
		t.Error("Error Edit Comment Article results")
		return
	}

	respDeleteCommentArticle, err := EditorServiceClient.DeleteCommentArticle(
		context.TODO(),
		&editor.DeleteCommentArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
			CommentId: secondCommentID.(primitive.ObjectID).Hex(),
		})
	if err != nil || respDeleteCommentArticle == nil || !respDeleteCommentArticle.Success {
		t.Errorf("Error Delete Comment Article response: %s", err)
		return
	}

	respGetArticle, err = EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != articleID.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if len(respGetArticle.Article.Comments) != 1 ||
		respGetArticle.Article.Comments[0].Content != "This is the first comment" {
		t.Error("Error Delete Comment Article results")
		return
	}

	respArchiveCommentArticle, err := EditorServiceClient.ArchiveCommentArticle(
		context.TODO(),
		&editor.ArchiveCommentArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
			CommentId: firstCommentID.(primitive.ObjectID).Hex(),
		})
	if err != nil || respArchiveCommentArticle == nil || !respArchiveCommentArticle.Success {
		t.Errorf("Error Archive Comment Article response: %s", err)
		return
	}

	respGetArticle, err = EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != articleID.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticle.Article.Comments != nil ||
		respGetArticle.Article.ArchiveComments[0].Content != "This is the first comment" {
		t.Error("Error Archive Comment Article results")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      author,
			ArticleId: articleID.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(userForCommenting.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (commentator)")
		return
	}

	userID, _ = primitive.ObjectIDFromHex(author.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}

func TestAppointmentToResponsibleEditor(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	user := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(user))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	var id interface{} = primitive.NewObjectID()

	respCreateArticle, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User: user,
			Article: &editor.Article{
				Id:        id.(primitive.ObjectID).Hex(),
				Status:    "Redaction",
				Author:    user,
				Name:      "article 1",
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
				Content:   "This is content article 1",
			},
		})
	if err != nil || respCreateArticle == nil || !respCreateArticle.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respAppointmentToResponsibleEditor, err := EditorServiceClient.AppointmentToResponsibleEditor(
		context.TODO(),
		&editor.AppointmentToResponsibleEditorRequest{
			User:              user,
			ArticleId:         id.(primitive.ObjectID).Hex(),
			ResponsibleEditor: user,
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if !respAppointmentToResponsibleEditor.Success {
		t.Error("Error Appointment To Responsible editor success")
		return
	}

	respGetArticle, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != id.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticle.Article.ResponsibleEditor.FirstName != user.FirstName ||
		respGetArticle.Article.ResponsibleEditor.Id != user.Id {
		t.Error("Error Appointment To Responsible editor result")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(user.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}

func TestAppointmentToSectionEditor(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	user := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(user))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	var id interface{} = primitive.NewObjectID()

	respCreateArticle, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User: user,
			Article: &editor.Article{
				Id:        id.(primitive.ObjectID).Hex(),
				Status:    "Redaction",
				Author:    user,
				Name:      "article 1",
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
				Content:   "This is content article 1",
				Section: &editor.Section{
					SectionEditor: nil,
					SectionAuthor: nil,
					Name:          "Section 1",
					Content:       "Content section 1",
				},
			},
		})
	if err != nil || respCreateArticle == nil || !respCreateArticle.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respAppointmentToSectionEditor, err := EditorServiceClient.AppointmentToSectionEditor(
		context.TODO(),
		&editor.AppointmentToSectionEditorRequest{
			User:          user,
			ArticleId:     id.(primitive.ObjectID).Hex(),
			SectionEditor: user,
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if !respAppointmentToSectionEditor.Success {
		t.Error("Error Appointment To Section editor success")
		return
	}

	respGetArticle, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != id.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticle.Article.Section.SectionEditor.FirstName != user.FirstName ||
		respGetArticle.Article.Section.Content != "Content section 1" {
		t.Error("Error Appointment To Section editor result")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(user.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}

func TestAppointmentToSectionAuthor(t *testing.T) {
	if EditorServiceClient == nil {
		err := createEditorServiceClient()
		if err != nil {
			t.Errorf("Error connections to Editor Service gRPC: %s", err)
			return
		}
	}

	user := &editor.User{
		Id:         "5f399a6bfa094f3ed4f44181",
		FirstName:  "Andrew",
		MiddleName: "Vladimirovich",
		LastName:   "Ivashenko",
		Role:       "",
	}

	err := userCollection.Create(model.RenderBackUserModel(user))
	if err != nil {
		t.Errorf("Error Create testing user (author): %s", err)
		return
	}

	var id interface{} = primitive.NewObjectID()

	respCreateArticle, err := EditorServiceClient.CreateArticle(
		context.TODO(),
		&editor.CreateArticleRequest{
			User: user,
			Article: &editor.Article{
				Id:        id.(primitive.ObjectID).Hex(),
				Status:    "Redaction",
				Author:    user,
				Name:      "article 1",
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
				Content:   "This is content article 1",
				Section: &editor.Section{
					SectionEditor: nil,
					SectionAuthor: nil,
					Name:          "Section 1",
					Content:       "Content section 1",
				},
			},
		})
	if err != nil || respCreateArticle == nil || !respCreateArticle.Success {
		t.Errorf("Error Create Article response: %s", err)
		return
	}

	respAppointmentToSectionAuthor, err := EditorServiceClient.AppointmentToSectionAuthor(
		context.TODO(),
		&editor.AppointmentToSectionAuthorRequest{
			User:          user,
			ArticleId:     id.(primitive.ObjectID).Hex(),
			SectionAuthor: user,
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if !respAppointmentToSectionAuthor.Success {
		t.Error("Error Appointment To Section Author success")
		return
	}

	respGetArticle, err := EditorServiceClient.GetArticle(
		context.TODO(),
		&editor.GetArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}
	if respGetArticle == nil || respGetArticle.Article.Id != id.(primitive.ObjectID).Hex() {
		t.Error("Error Get Article response")
		return
	}
	if respGetArticle.Article.Section.SectionAuthor.FirstName != user.FirstName ||
		respGetArticle.Article.Section.Content != "Content section 1" {
		t.Error("Error Appointment To Section Author result")
		return
	}

	respDeleteArticle, err := EditorServiceClient.DeleteArticle(
		context.TODO(),
		&editor.DeleteArticleRequest{
			User:      user,
			ArticleId: id.(primitive.ObjectID).Hex(),
		})
	if err != nil {
		t.Error("Error Delete Article response")
		return
	}
	if !respDeleteArticle.Access {
		t.Error("Error Delete Article access")
		return
	}

	userID, _ := primitive.ObjectIDFromHex(user.Id)
	err = userCollection.Delete(userID)
	if err != nil {
		t.Error("Error deleting testing user (author)")
		return
	}
}
