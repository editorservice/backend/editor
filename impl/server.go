package impl

import (
	"context"
	log "github.com/sirupsen/logrus"
	editor "gitlab.com/editorservice/backend/editor/api"
	"gitlab.com/editorservice/backend/editor/db"
	"gitlab.com/editorservice/backend/editor/errorservise"
	"google.golang.org/grpc"
	"net"
)

var (
	userCollection    = db.GetUserDB(context.TODO())
	articleCollection = db.GetArticleDB(context.TODO())
)

type Server interface {
	editor.EditorServiceServer
	Serve(addr string)
}

type serverImpl struct {
}

func NewServer() Server {
	server := &serverImpl{}
	return server
}

func (server *serverImpl) Serve(addr string) {
	if listener, err := net.Listen("tcp", addr); err != nil {
		panic(errorservise.CreateServerError)
	} else {
		grpcServer := grpc.NewServer()
		editor.RegisterEditorServiceServer(grpcServer, server)
		log.Infof("Editor Service started on %s", addr)
		if err := grpcServer.Serve(listener); err != nil {
			panic(errorservise.ErrorServeServer)
		}
	}
}
