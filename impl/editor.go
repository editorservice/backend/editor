package impl

import (
	"context"
	"github.com/pkg/errors"
	editor "gitlab.com/editorservice/backend/editor/api"
	"gitlab.com/editorservice/backend/editor/errorservise"
	"gitlab.com/editorservice/backend/editor/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func (server *serverImpl) GetArticle(ctx context.Context, request *editor.GetArticleRequest) (*editor.GetArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.GetArticleResponse{
			Article: nil,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.GetArticleId())
	article, err := articleCollection.Get(id)

	if err != nil {
		return &editor.GetArticleResponse{
			Article: nil,
		}, err
	}

	if article != nil {
		return &editor.GetArticleResponse{
			Article: model.RenderArticleModel(article),
		}, nil
	} else {
		return &editor.GetArticleResponse{
			Article: nil,
		}, err
	}
}

func (server *serverImpl) GetListArticle(ctx context.Context, request *editor.GetListArticleRequest) (*editor.GetListArticleResponse, error) {
	articles, err := articleCollection.GetList()

	if err != nil {
		return nil, err
	}

	return &editor.GetListArticleResponse{
		Article: model.RenderListArticleModel(articles),
	}, nil
}

func (server *serverImpl) CreateArticle(ctx context.Context, request *editor.CreateArticleRequest) (*editor.CreateArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.CreateArticleResponse{
			Success: false,
		}, err
	}

	newArticle := model.RenderBackArticleModel(request.Article)
	err = articleCollection.Create(newArticle)

	if err != nil {
		return &editor.CreateArticleResponse{
			Success: false,
		}, err
	}

	return &editor.CreateArticleResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) DeleteArticle(ctx context.Context, request *editor.DeleteArticleRequest) (*editor.DeleteArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.DeleteArticleResponse{
			Access: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.GetArticleId())
	err = articleCollection.Delete(id)

	if err != nil {
		return &editor.DeleteArticleResponse{
			Access: true,
		}, err
	}

	return &editor.DeleteArticleResponse{
		Access: true,
	}, nil
}

func (server *serverImpl) EditArticle(ctx context.Context, request *editor.EditArticleRequest) (*editor.EditArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.EditArticleResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.GetArticle().GetId())

	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.EditArticleResponse{
			Success: false,
		}, err
	}
	updatedArticle := model.RenderBackArticleModel(request.GetArticle())
	updatedArticle.CurrentVersion += 1

	lastVersion := &model.Version{
		Article:   article,
		CreatedAt: article.CreatedAt,
	}
	updatedArticle.Versions = append(updatedArticle.Versions, lastVersion)
	updatedArticle.UpdatedAt = time.Now().Format("2 January 2006 15:04")

	err = articleCollection.Update(updatedArticle)
	if err != nil {
		return &editor.EditArticleResponse{
			Success: false,
		}, err
	}
	return &editor.EditArticleResponse{
		Success: true,
	}, err
}

func (server *serverImpl) InsertToArticle(ctx context.Context, request *editor.InsertToArticleRequest) (*editor.InsertToArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.InsertToArticleResponse{
			Success:   false,
			ArticleId: "",
		}, err
	}

	mainArticleID, _ := primitive.ObjectIDFromHex(request.GetMainArticleId())
	attachedArticleID, _ := primitive.ObjectIDFromHex(request.GetNewArticle().GetId())

	article, err := articleCollection.Get(mainArticleID)
	if err != nil || article == nil {
		return &editor.InsertToArticleResponse{
			Success:   false,
			ArticleId: "",
		}, err
	}
	article.AttachedArticles = append(
		article.AttachedArticles,
		model.RenderBackArticleModel(request.GetNewArticle()))

	err = articleCollection.Update(article)
	if err != nil {
		return &editor.InsertToArticleResponse{
			Success:   false,
			ArticleId: "",
		}, err
	}

	err = articleCollection.Delete(attachedArticleID)
	if err != nil {
		return &editor.InsertToArticleResponse{
			Success:   false,
			ArticleId: mainArticleID.String(),
		}, err
	}

	return &editor.InsertToArticleResponse{
		Success:   true,
		ArticleId: mainArticleID.String(),
	}, nil
}

func (server *serverImpl) CommentArticle(ctx context.Context, request *editor.CommentArticleRequest) (*editor.CommentArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.CommentArticleResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.GetArticleId())

	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.CommentArticleResponse{
			Success: false,
		}, err
	}

	article.Comments = append(article.Comments, model.RenderBackArticleCommentModel(request.GetComment()))
	err = articleCollection.Update(article)

	if err != nil {
		return &editor.CommentArticleResponse{
			Success: false,
		}, err
	}

	return &editor.CommentArticleResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) EditCommentArticle(ctx context.Context, request *editor.EditCommentArticleRequest) (*editor.EditCommentArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.EditCommentArticleResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.EditCommentArticleResponse{
			Success: false,
		}, err
	}

	commentID, _ := primitive.ObjectIDFromHex(request.Comment.Id)
	for i, comment := range article.Comments {
		if comment.Id == commentID {
			article.Comments[i].Content = request.Comment.Content
			article.Comments[i].Author = model.RenderBackUserModel(request.Comment.Author)
		}
	}
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.EditCommentArticleResponse{
			Success: false,
		}, err
	}

	return &editor.EditCommentArticleResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) DeleteCommentArticle(ctx context.Context, request *editor.DeleteCommentArticleRequest) (*editor.DeleteCommentArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.DeleteCommentArticleResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.DeleteCommentArticleResponse{
			Success: false,
		}, err
	}

	commentID, _ := primitive.ObjectIDFromHex(request.CommentId)
	for i, comment := range article.Comments {
		if comment.Id == commentID {
			article.Comments = append(article.Comments[:i], article.Comments[i+1:]...)
		}
	}
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.DeleteCommentArticleResponse{
			Success: false,
		}, err
	}

	return &editor.DeleteCommentArticleResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) ArchiveCommentArticle(ctx context.Context, request *editor.ArchiveCommentArticleRequest) (*editor.ArchiveCommentArticleResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.ArchiveCommentArticleResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.ArchiveCommentArticleResponse{
			Success: false,
		}, err
	}

	commentID, _ := primitive.ObjectIDFromHex(request.CommentId)
	for i, comment := range article.Comments {
		if comment.Id == commentID {
			article.ArchiveComments = append(
				article.ArchiveComments,
				&model.Comment{
					Id:      comment.Id,
					Author:  comment.Author,
					Content: comment.Content,
				})
			article.Comments = append(article.Comments[:i], article.Comments[i+1:]...)
		}
	}
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.ArchiveCommentArticleResponse{
			Success: false,
		}, err
	}

	return &editor.ArchiveCommentArticleResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) AppointmentToResponsibleEditor(ctx context.Context, request *editor.AppointmentToResponsibleEditorRequest) (*editor.AppointmentToResponsibleEditorResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.AppointmentToResponsibleEditorResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.AppointmentToResponsibleEditorResponse{
			Success: false,
		}, err
	}
	article.ResponsibleEditor = model.RenderBackUserModel(request.User)
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.AppointmentToResponsibleEditorResponse{
			Success: false,
		}, err
	}

	return &editor.AppointmentToResponsibleEditorResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) AppointmentToSectionEditor(ctx context.Context, request *editor.AppointmentToSectionEditorRequest) (*editor.AppointmentToSectionEditorResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.AppointmentToSectionEditorResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.AppointmentToSectionEditorResponse{
			Success: false,
		}, err
	}
	article.Section.SectionEditor = model.RenderBackUserModel(request.User)
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.AppointmentToSectionEditorResponse{
			Success: false,
		}, err
	}

	return &editor.AppointmentToSectionEditorResponse{
		Success: true,
	}, nil
}

func (server *serverImpl) AppointmentToSectionAuthor(ctx context.Context, request *editor.AppointmentToSectionAuthorRequest) (*editor.AppointmentToSectionAuthorResponse, error) {
	//TODO: check user's role
	requiredRole := ""
	access, err := checkRole(userCollection, request.GetUser().GetId(), requiredRole)
	if !access {
		return &editor.AppointmentToSectionAuthorResponse{
			Success: false,
		}, err
	}

	id, _ := primitive.ObjectIDFromHex(request.ArticleId)
	article, err := articleCollection.Get(id)
	if err != nil || article == nil {
		return &editor.AppointmentToSectionAuthorResponse{
			Success: false,
		}, err
	}
	article.Section.SectionAuthor = model.RenderBackUserModel(request.User)
	err = articleCollection.Update(article)
	if err != nil {
		return &editor.AppointmentToSectionAuthorResponse{
			Success: false,
		}, err
	}

	return &editor.AppointmentToSectionAuthorResponse{
		Success: true,
	}, nil
}

//TODO: check user's role
func checkRole(userCollection model.DBInterfaceUser, userID string, requiredRole string) (bool, error) {
	id, _ := primitive.ObjectIDFromHex(userID)
	role, err := userCollection.GetRole(id)
	if err != nil || role != requiredRole {
		return false, errors.Errorf(errorservise.ErrorToAccess)
	}

	return true, nil
}
