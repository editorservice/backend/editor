package model

import (
	editor "gitlab.com/editorservice/backend/editor/api"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Article struct {
	Id                primitive.ObjectID `bson:"_id"`
	Status            string             `bson:"status"`
	Author            *User              `bson:"author"`
	Name              string             `bson:"name"`
	CreatedAt         string             `bson:"created_at"`
	UpdatedAt         string             `bson:"updated_at"`
	Content           string             `bson:"content"`
	Comments          []*Comment         `bson:"comments"`
	ArchiveComments   []*Comment         `bson:"archive_comments"`
	Versions          []*Version         `bson:"versions"`
	TemplateID        primitive.ObjectID `bson:"template_id"`
	AttachedArticles  []*Article         `bson:"attached_articles"`
	CurrentVersion    int64              `bson:"current_version"`
	ResponsibleEditor *User              `bson:"responsible_editor"`
	Section           *Section           `bson:"section"`
	Reviewer          *User              `bson:"reviewer"`
	Review            string             `bson:"review"`
	CuratorConsultant *User              `bson:"curator_consultant"`
}

type Section struct {
	SectionEditor *User  `bson:"section_editor"`
	SectionAuthor *User  `bson:"section_author"`
	Name          string `bson:"name"`
	Content       string `bson:"content"`
}

type Version struct {
	Article   *Article `bson:"article"`
	CreatedAt string   `bson:"created_at"`
}

type Comment struct {
	Id      primitive.ObjectID `bson:"_id"`
	Author  *User              `bson:"author"`
	Content string             `bson:"content"`
}

func RenderArticleCommentModel(model *Comment) *editor.Comment {
	result := &editor.Comment{}
	var commentID interface{}
	commentID = model.Id
	result = &editor.Comment{
		Id:      commentID.(primitive.ObjectID).Hex(),
		Author:  RenderUserModel(model.Author),
		Content: model.Content,
	}
	return result
}

func RenderBackArticleCommentModel(model *editor.Comment) *Comment {
	result := &Comment{}
	commentID, _ := primitive.ObjectIDFromHex(model.GetId())
	result = &Comment{
		Id:      commentID,
		Author:  RenderBackUserModel(model.GetAuthor()),
		Content: model.GetContent(),
	}
	return result
}

func RenderArticleListCommentModel(model []*Comment) []*editor.Comment {
	var (
		commentID interface{}
		comments  []*editor.Comment
	)

	for _, c := range model {
		commentID = c.Id

		comments = append(comments, &editor.Comment{
			Id:      commentID.(primitive.ObjectID).Hex(),
			Author:  RenderUserModel(c.Author),
			Content: c.Content,
		})
	}
	return comments
}

func RenderBackArticleListCommentModel(model []*editor.Comment) []*Comment {
	var comments []*Comment

	for _, c := range model {
		commentID, _ := primitive.ObjectIDFromHex(c.GetId())

		comments = append(comments, &Comment{
			Id:      commentID,
			Author:  RenderBackUserModel(c.GetAuthor()),
			Content: c.GetContent(),
		})
	}
	return comments
}

func RenderArticleVersionModel(model []*Version) []*editor.VersionArticle {
	var versions []*editor.VersionArticle

	for _, v := range model {
		versions = append(versions, &editor.VersionArticle{
			Article:   RenderArticleModel(v.Article),
			CreatedAt: v.CreatedAt,
		})
	}
	return versions
}

func RenderBackArticleVersionModel(model []*editor.VersionArticle) []*Version {
	var versions []*Version

	for _, v := range model {
		versions = append(versions, &Version{
			Article:   RenderBackArticleModel(v.GetArticle()),
			CreatedAt: v.GetCreatedAt(),
		})
	}
	return versions
}

func RenderSectionArticleModel(model *Section) *editor.Section {
	result := &editor.Section{}
	if model != nil {
		result = &editor.Section{
			SectionEditor: RenderUserModel(model.SectionEditor),
			SectionAuthor: RenderUserModel(model.SectionAuthor),
			Name:          model.Name,
			Content:       model.Content,
		}
	}
	return result
}

func RenderBackSectionArticleModel(model *editor.Section) *Section {
	result := &Section{}
	result = &Section{
		SectionEditor: RenderBackUserModel(model.GetSectionEditor()),
		SectionAuthor: RenderBackUserModel(model.GetSectionAuthor()),
		Name:          model.GetName(),
		Content:       model.GetContent(),
	}
	return result
}

func RenderListArticleModel(model []*Article) []*editor.Article {
	var versions []*editor.Article

	for _, a := range model {
		versions = append(versions, RenderArticleModel(a))
	}
	return versions
}

func RenderBackListArticleModel(model []*editor.Article) []*Article {
	var versions []*Article

	for _, a := range model {
		versions = append(versions, RenderBackArticleModel(a))
	}
	return versions
}

func RenderBackArticleModel(model *editor.Article) *Article {
	result := &Article{}
	articleID, _ := primitive.ObjectIDFromHex(model.GetId())
	templateID, _ := primitive.ObjectIDFromHex(model.GetTemplateId())

	result = &Article{
		Id:                articleID,
		Status:            model.GetStatus(),
		Author:            RenderBackUserModel(model.GetAuthor()),
		Name:              model.GetName(),
		CreatedAt:         model.GetCreatedAt(),
		UpdatedAt:         model.GetUpdatedAt(),
		Content:           model.GetContent(),
		Comments:          RenderBackArticleListCommentModel(model.GetComments()),
		ArchiveComments:   RenderBackArticleListCommentModel(model.GetComments()),
		Versions:          RenderBackArticleVersionModel(model.GetVersions()),
		TemplateID:        templateID,
		AttachedArticles:  RenderBackListArticleModel(model.GetAttachedArticles()),
		CurrentVersion:    model.GetCurrentVersion(),
		ResponsibleEditor: RenderBackUserModel(model.GetResponsibleEditor()),
		Reviewer:          RenderBackUserModel(model.GetReviewer()),
		Review:            model.GetReview(),
		CuratorConsultant: RenderBackUserModel(model.GetCuratorConsultant()),
		Section:           RenderBackSectionArticleModel(model.GetSection()),
	}
	return result
}

func RenderArticleModel(model *Article) *editor.Article {
	result := &editor.Article{}
	var (
		articleID  interface{} = model.Id
		templateID interface{} = model.TemplateID
	)

	result = &editor.Article{
		Id:                articleID.(primitive.ObjectID).Hex(),
		Status:            model.Status,
		Author:            RenderUserModel(model.Author),
		Name:              model.Name,
		CreatedAt:         model.CreatedAt,
		UpdatedAt:         model.UpdatedAt,
		Content:           model.Content,
		Comments:          RenderArticleListCommentModel(model.Comments),
		ArchiveComments:   RenderArticleListCommentModel(model.ArchiveComments),
		Versions:          RenderArticleVersionModel(model.Versions),
		TemplateId:        templateID.(primitive.ObjectID).Hex(),
		AttachedArticles:  RenderListArticleModel(model.AttachedArticles),
		CurrentVersion:    model.CurrentVersion,
		ResponsibleEditor: RenderUserModel(model.ResponsibleEditor),
		Reviewer:          RenderUserModel(model.Reviewer),
		Review:            model.Review,
		CuratorConsultant: RenderUserModel(model.CuratorConsultant),
		Section:           RenderSectionArticleModel(model.Section),
	}
	return result
}

type DBInterfaceArticle interface {
	Create(article *Article) error
	Get(id primitive.ObjectID) (*Article, error)
	GetByVersion(id primitive.ObjectID, versionNumber int64) (*Article, error)
	GetList() ([]*Article, error)
	Update(article *Article) error
	Delete(id primitive.ObjectID) error
}
