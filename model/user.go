package model

import (
	editor "gitlab.com/editorservice/backend/editor/api"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	Id         primitive.ObjectID `bson:"_id"`
	FirstName  string             `bson:"first_name"`
	MiddleName string             `bson:"middle_name"`
	LastName   string             `bson:"last_name"`
	Role       string             `bson:"role"`
}

func RenderUserModel(model *User) *editor.User {
	result := &editor.User{}
	if model != nil {
		var userID interface{}
		userID = model.Id

		result = &editor.User{
			Id:         userID.(primitive.ObjectID).Hex(),
			FirstName:  model.FirstName,
			MiddleName: model.MiddleName,
			LastName:   model.LastName,
			Role:       model.Role,
		}
	}
	return result
}

func RenderBackUserModel(model *editor.User) *User {
	result := &User{}
	userID, _ := primitive.ObjectIDFromHex(model.GetId())

	result = &User{
		Id:         userID,
		FirstName:  model.GetFirstName(),
		MiddleName: model.GetMiddleName(),
		LastName:   model.GetLastName(),
		Role:       model.GetRole(),
	}
	return result
}

type DBInterfaceUser interface {
	FindByID(id primitive.ObjectID) (*User, error)
	GetRole(id primitive.ObjectID) (string, error)
	Create(user *User) error
	Delete(id primitive.ObjectID) error
}
