package model

import (
	editor "gitlab.com/editorservice/backend/editor/api"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
	"time"
)

func TestArticleRendering(t *testing.T) {
	var (
		modelArticleID              = primitive.NewObjectID()
		modelUserID                 = primitive.NewObjectID()
		modelCommentID              = primitive.NewObjectID()
		editorArticleID interface{} = modelArticleID
		editorUserID    interface{} = modelUserID
		editorCommentID interface{} = modelCommentID
	)
	editorArticle := &editor.Article{
		Id:     editorArticleID.(primitive.ObjectID).Hex(),
		Status: "Redaction",
		Author: &editor.User{
			Id:        editorUserID.(primitive.ObjectID).Hex(),
			FirstName: "Alex",
			LastName:  "Ivanov",
		},
		Name:      "Article 1",
		CreatedAt: time.Now().Format("2 January 2006 15:04"),
		Content:   "This is content",
		Comments: []*editor.Comment{
			{
				Id: editorCommentID.(primitive.ObjectID).Hex(),
				Author: &editor.User{
					Id:        editorUserID.(primitive.ObjectID).Hex(),
					FirstName: "Alex",
					LastName:  "Ivanov",
				},
				Content: "This is comments content",
			},
			{
				Id: editorCommentID.(primitive.ObjectID).Hex(),
				Author: &editor.User{
					Id:        editorUserID.(primitive.ObjectID).Hex(),
					FirstName: "Alex",
					LastName:  "Ivanov",
				},
				Content: "This is comments content",
			},
		},
		Versions: []*editor.VersionArticle{
			{
				Article: &editor.Article{
					Id:     editorArticleID.(primitive.ObjectID).Hex(),
					Status: "Redaction",
					Name:   "Article 1, first version",
				},
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
			},
			{
				Article: &editor.Article{
					Id:     editorArticleID.(primitive.ObjectID).Hex(),
					Status: "Redaction",
					Name:   "Article 1, second version",
				},
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
			},
		},
		Section: &editor.Section{
			SectionEditor: &editor.User{
				Id:        editorUserID.(primitive.ObjectID).Hex(),
				FirstName: "Alex",
				LastName:  "Ivanov",
			},
			SectionAuthor: &editor.User{
				Id:        editorUserID.(primitive.ObjectID).Hex(),
				FirstName: "Alex",
				LastName:  "Ivanov",
			},
			Name:    "Section 1",
			Content: "Content section 1",
		},
	}
	modelArticle := &Article{
		Id:     modelArticleID,
		Status: "Redaction",
		Author: &User{
			Id:        modelUserID,
			FirstName: "Alex",
			LastName:  "Ivanov",
		},
		Name:      "Article 1",
		CreatedAt: time.Now().Format("2 January 2006 15:04"),
		Content:   "This is content",
		Comments: []*Comment{
			{
				Id: modelCommentID,
				Author: &User{
					Id:        modelUserID,
					FirstName: "Alex",
					LastName:  "Ivanov",
				},
				Content: "This is comments content",
			},
			{
				Id: modelCommentID,
				Author: &User{
					Id:        modelUserID,
					FirstName: "Alex",
					LastName:  "Ivanov",
				},
				Content: "This is comments content",
			},
		},
		Versions: []*Version{
			{
				Article: &Article{
					Id:     modelArticleID,
					Status: "Redaction",
					Name:   "Article 1, first version",
				},
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
			},
			{
				Article: &Article{
					Id:     modelArticleID,
					Status: "Redaction",
					Name:   "Article 1, second version",
				},
				CreatedAt: time.Now().Format("2 January 2006 15:04"),
			},
		},
		Section: &Section{
			SectionEditor: &User{
				Id:        modelUserID,
				FirstName: "Alex",
				LastName:  "Ivanov",
			},
			SectionAuthor: &User{
				Id:        modelUserID,
				FirstName: "Alex",
				LastName:  "Ivanov",
			},
			Name:    "Section 1",
			Content: "Content section 1",
		},
	}

	renderingEditorArticle := RenderBackArticleModel(editorArticle)
	renderingModelArticle := RenderArticleModel(modelArticle)

	if renderingEditorArticle.Content != modelArticle.Content ||
		renderingEditorArticle.Author.FirstName != modelArticle.Author.FirstName ||
		renderingEditorArticle.Comments[0].Author.LastName != modelArticle.Comments[0].Author.LastName ||
		renderingEditorArticle.Comments[0].Id != modelArticle.Comments[0].Id ||
		renderingEditorArticle.Section.SectionAuthor.FirstName != modelArticle.Section.SectionAuthor.FirstName ||
		renderingEditorArticle.Versions[0].Article.Name != modelArticle.Versions[0].Article.Name {
		t.Error("Error RenderBackArticleModel")
		return
	}
	if renderingModelArticle.Content != editorArticle.Content ||
		renderingModelArticle.Author.FirstName != editorArticle.Author.FirstName ||
		renderingModelArticle.Comments[0].Author.LastName != editorArticle.Comments[0].Author.LastName ||
		renderingModelArticle.Comments[0].Id != editorArticle.Comments[0].Id ||
		renderingModelArticle.Section.SectionAuthor.FirstName != editorArticle.Section.SectionAuthor.FirstName ||
		renderingModelArticle.Versions[0].Article.Name != editorArticle.Versions[0].Article.Name {
		t.Error("Error RenderArticleModel")
		return
	}
}
