module gitlab.com/editorservice/backend/editor

go 1.14

replace gitlab.com/editorservice/backend/editor/api => ./api

require (
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/editorservice/backend/editor/api v0.0.0-20200813164633-38747b827c3e
	go.mongodb.org/mongo-driver v1.4.0
	google.golang.org/grpc v1.31.0
)
