package connections

import (
	"os"
)

const (
	NameDB            = "editor"
	UserCollection    = "user"
	ArticleCollection = "article"
)

//for deploy - "mongodb://mongo:27017"
//for testing on localhost - "mongodb://localhost:27017"

var (
	EditorServiceUrl = getEnv("EDITOR_SERVICE_URL", ":10000")
	MongoConnect     = getEnv("MONGO_CONNECT", "mongodb://localhost:27017")
)

func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultVal
}
