//go:generate go mod vendor
//go:generate protoc --go_out=plugins=grpc,paths=source_relative:. -I .:vendor/gitlab.com/editorservice/backend api.proto
//go:generate rm -r vendor
//go:generate go fmt ./...
//go:generate go fix ./...

package editor

import (
// generic proto
)
