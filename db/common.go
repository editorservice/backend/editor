package db

import (
	"context"
	"gitlab.com/editorservice/backend/editor/errorservise"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func GetMongoCollection(ctx context.Context, mongoConnect, nameDB, collection string) *mongo.Collection {
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoConnect))

	if err != nil {
		panic(errorservise.CreateClientDB)
	}
	err = client.Connect(ctx)
	if err != nil {
		panic(errorservise.ErrorConnectDB)
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		panic(errorservise.PingDataBaseConnectError)
	}

	return client.Database(nameDB).Collection(collection)
}
