package db

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/editorservice/backend/editor/connections"
	"gitlab.com/editorservice/backend/editor/errorservise"
	"gitlab.com/editorservice/backend/editor/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type user struct {
	collection *mongo.Collection
}

func (u *user) FindByID(id primitive.ObjectID) (*model.User, error) {
	var (
		res    *model.User
		filter = bson.D{{"_id", id}}
	)
	err := u.collection.FindOne(context.TODO(), filter).Decode(&res)
	if err != nil {
		return &model.User{}, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return res, nil
}

func (u *user) GetRole(id primitive.ObjectID) (string, error) {
	foundUser, err := u.FindByID(id)
	if err != nil {
		return "", errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return foundUser.Role, nil
}

func (u *user) Create(user *model.User) error {
	_, err := u.collection.InsertOne(context.TODO(), user)
	if err != nil {
		return errors.Errorf(errorservise.ErrorRequestResponseDB)
	} else {
		return nil
	}
}

func (u *user) Delete(id primitive.ObjectID) error {
	var filter = bson.D{{"_id", id}}
	_, err := u.collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return errors.Errorf(errorservise.ErrorRequestResponseDB)
	} else {
		return nil
	}
}

func GetUserDB(ctx context.Context) model.DBInterfaceUser {
	collection := GetMongoCollection(
		ctx,
		connections.MongoConnect,
		connections.NameDB,
		connections.UserCollection)
	return &user{collection: collection}
}
