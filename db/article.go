package db

import (
	"context"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/editorservice/backend/editor/connections"
	"gitlab.com/editorservice/backend/editor/errorservise"
	"gitlab.com/editorservice/backend/editor/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type article struct {
	collection *mongo.Collection
}

func (a *article) Create(article *model.Article) error {
	_, err := a.collection.InsertOne(context.TODO(), article)
	if err != nil {
		return errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return nil
}

func (a *article) Get(id primitive.ObjectID) (*model.Article, error) {
	var (
		res    *model.Article
		filter = bson.D{{"_id", id}}
	)
	err := a.collection.FindOne(context.TODO(), filter).Decode(&res)
	if err != nil {
		return &model.Article{}, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return res, nil
}

func (a *article) GetList() ([]*model.Article, error) {
	var res []*model.Article
	opts := options.Find()
	cur, err := a.collection.Find(context.TODO(), nil, opts)
	if err != nil {
		return nil, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	err = cur.All(context.TODO(), &res)
	if err != nil {
		return nil, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return res, nil
}

func (a *article) GetByVersion(id primitive.ObjectID, versionNumber int64) (*model.Article, error) {
	var (
		res    *model.Article
		filter = bson.D{{"_id", id}}
	)
	err := a.collection.FindOne(context.TODO(), filter).Decode(&res)
	if err != nil {
		return &model.Article{}, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	if res.Versions != nil {
		if versionNumber > int64(len(res.Versions)) {
			return nil, errors.Errorf(errorservise.ErrorRequestResponseDB)
		}
	} else {
		return nil, errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	return res.Versions[versionNumber].Article, nil
}

func (a *article) Update(article *model.Article) error {
	filter := bson.D{{"_id", article.Id}}
	update := bson.D{{"$set", article}}
	_, err := a.collection.UpdateOne(
		context.TODO(),
		filter,
		update,
	)
	if err != nil {
		return errors.Errorf(errorservise.ErrorRequestResponseDB)
	} else {
		return nil
	}
}

func (a *article) Delete(id primitive.ObjectID) error {
	var filter = bson.D{{"_id", id}}
	opts := options.Delete()
	_, err := a.collection.DeleteOne(context.TODO(), filter, opts)
	if err != nil {
		return errors.Errorf(errorservise.ErrorRequestResponseDB)
	}
	log.Infof("Article with id - %v is deleted", id)
	return nil
}

func GetArticleDB(ctx context.Context) model.DBInterfaceArticle {
	collection := GetMongoCollection(
		ctx,
		connections.MongoConnect,
		connections.NameDB,
		connections.ArticleCollection)
	return &article{collection: collection}
}
