package errorservise

const (
	CreateServerError        = "Error code 1: Error to creating gRPC server"
	ErrorServeServer         = "Error code 2: Error serving server"
	ErrorConnectDB           = "Error code 3: No connection with mongoDB"
	CreateClientDB           = "Error code 4: Error to creating mongoDB client"
	PingDataBaseConnectError = "Error code 5: Ping DB connect error"
	ErrorToAccess            = "Error code 6: Error to accessing resource"
	ErrorRequestResponseDB   = "Error code 7: Error with requests or responses DB"
)
